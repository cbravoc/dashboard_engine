$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "dashboard_engine/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "dashboard_engine"
  s.version     = DashboardEngine::VERSION
  s.authors     = ["CBravo"]
  s.email       = ["magno2005@gmail.com"]
  s.homepage    = ""
  s.summary     = "Summary of DashboardEngine."
  s.description = "Description of DashboardEngine."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.2.4"

  s.add_development_dependency "sqlite3"

  s.add_dependency "jquery-rails"

  s.add_dependency "chartkick"
end
