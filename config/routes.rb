DashboardEngine::Engine.routes.draw do
  root :to => 'dashboard#index'

  get 'dashboard/index'
  get 'dashboard/charts'
  get 'dashboard/tables'
  get 'dashboard/extras'

  get "projects/:id" => "dashboard#index"
  get "projects/:id/ppc" => "dashboard#charts"
  get "projects/:id/pcr" => "dashboard#tables"
  get "projects/:id/cnc" => "dashboard#extras"

  #get 'dashboard/users'
  #get 'dashboard/forms'
  #get 'dashboard/gallery'
  #get 'dashboard/calendar'
  #get 'dashboard/ui_elements'
  #get 'dashboard/my_info'
  #get 'dashboard/new_user'
  #get 'dashboard/user_profile'
  #get 'dashboard/form_wizard'
  #get 'dashboard/datatables'
  #get 'dashboard/icons'
  #get 'dashboard/code_editor'
  #get 'dashboard/grids'
  #get 'dashboard/signin'
  #get 'dashboard/signup'
  #get 'dashboard/iframe'
end