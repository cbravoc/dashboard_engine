require "dashboard_engine/engine"

module DashboardEngine
	class Configuration
    attr_accessor :configurable_service
  end
  class << self
    attr_writer :configuration
  end

  module_function
  def configuration
    @configuration ||= Configuration.new
  end

  def configure
    yield(configuration)
  end
end